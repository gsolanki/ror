require './tax.rb'
require './item.rb'
require './calculator.rb'

ORDER_1 = ['1 book at 12.49', '1 music CD at 14.99', '1 chocolate bar at 0.85']

ORDER_2 = ['1 imported box of chocolates at 10.00', '1 imported bottle of perfume at 47.50']

ORDER_3 = ['1 imported bottle of perfume at 27.99', '1 perfume at 18.99', '1 pills at 9.75', '1 imported box of chocolates at 11.25']

ORDERS = [ORDER_1, ORDER_2, ORDER_3]

tax_free_items = ["book", "chocolate", "chocolates", "pills"]

class Order
  include Tax, Calculator
  attr_accessor :total_tax, :total_bill, :imported, :item_description, :item_name, :price, :quantity, :tax_free, :product
  def initialize(order, tax_free_items)
    self.total_tax = 0
    self.total_bill = 0
    order.each do |item|
      self.product = Item.new
      self.imported = self.product.imported?(item)
      self.item_description = self.product.get_name(item, self.imported)
      self.item_name = self.item_description.split()[-1]
      self.price = self.product.get_price(item)
      self.quantity = self.product.get_quantity(item)
      self.tax_free = tax_free_items.include?(self.item_name)
      self.sales_tax = sales_tax(self.imported, self.tax_free)
      self.item_price = self.product.total_item_price(self.price, quantity, self.sales_tax)
    end
  end

  def generate_order_details(order, tax_free_items) 
    order.each do |item|
            
      self.total_tax += calculate_tax(self.price, self.quantity, self.sales_tax) 
      self.total_bill += self.item_price 
      print_order_receipt(self.item_description, self.item_price)
    end
      print_amount_receipt(self.total_tax, self.total_bill)
  end

  private
  def print_order_receipt(item_description, item_price)
    receipt = ''
    receipt.concat(item_description +" : "+ item_price.to_s+ "\n")    
    print receipt  
  end

  private
  def print_amount_receipt(total_tax, total_bill)
    receipt = ''
    receipt.concat("\nSales Tax: " + self.total_tax.to_s)               
    receipt.concat("\nTotal: " + self.total_bill.to_s + "\n")
    print receipt
    puts "\n----------------------------------------------------------------------------"
  end

end



