
module Tax

  def self.sales_tax(quantity, price, imported, item, tax_free)          #
    if tax_free
      self.tax_on_tax_free_items(imported)
    elsif imported
      15
    else
      10
    end
  end

  private
  def self.tax_on_tax_free_items(imported)             #denotes tax on tax free items
    if imported
      5
    else
      0
    end
  end
  
end



